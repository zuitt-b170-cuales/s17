console.log("Hello World!");

let students = ["Marie", "Carlos", "Mandy"];

function addStudent(element){
	students.push(element);
};

addStudent("Owen");

console.log(students);

function addStudents(){
	console.log(students.length);
};

addStudents();

/*let sortedStudents = students.sort();
console.log(sortedStudents);

sortedStudents.forEach(
		function(element){
			console.log(element);
		}
	);*/

function printStudents(){
	students.sort();

	students.forEach(
			function(element){
				console.log(element);
			}
		)
};

printStudents();
addStudent("Carlos");

function findStudent(element){
	if (students.indexOf(element) === -1) {
		console.log(`${element} is NOT an enrollee`);
	} 
	if (students.includes(element)) {
		console.log(`${element}, ${element} are enrollees`);
	} 
	else {
		console.log(`${element} is an enrollee`);
	}
};

console.log(students);
findStudent("Owen");




/*if (students.indexOf("CAN") === -1) {
	console.log("Element not existing");
}else{
	console.log("Element exists in the array");
}*/