console.log("1. drink html");
console.log("2. eat javascript");
console.log("3. inhale css");
console.log("4. bake bootstrap");


/*
	Arrays - are used to store multiple related data values inside a single variable. Arrays are declared using the square brackets ([]).
		SYNTAX:
			let/const <arrayName> = ["elementA", "elementB", ... "elementN"];
	Arrays are used if there is a need to manipulate the related values stored in it.

	index - position of each element in the array. Calling an element through the index is determined by using this syntax
		arrayName[index]

	REMINDER: the index is always starting with 0. Counting the elements inside would start with 0 instead of 1.
		formula: -nth element -1/arrayName.length -1
				 -start counting from 0
*/

let tasks = ["drink html", "eat javascript", "inhale css", "bake bootstrap"];

console.log(tasks);
console.log(tasks[2]);
console.log(tasks.length);

// Array Manipulation
let numbers = ["one", "two", "three", "four"];
console.log(numbers);
// console.log(numbers[4]);

// using assignment operator
numbers[4] = "five";
console.log(numbers);

// push method
numbers.push("element");
console.log(numbers);

// callback function
function pushMethod(element){
	numbers.push(element);
};

pushMethod("six");
pushMethod("seven");
pushMethod("eight");
console.log(numbers);

// Removing of an element
// pop method
numbers.pop();
console.log(numbers);

function popMethod(){
	numbers.pop();
};

popMethod();
popMethod();
/*popMethod();
popMethod();
popMethod();
popMethod();
popMethod();*/
console.log(numbers);


// ================
// manipulating the beginning / start of the array
// remove an element
// shift method - remove the first element of the array

numbers.shift();
console.log(numbers);

//callback function
function shiftMethod(){
	numbers.shift();
};

shiftMethod();
console.log(numbers);

// ================
// Adding an element
// unshift method
numbers.unshift("zero");
console.log(numbers);

// callback function
function unshiftMethod(element){
	numbers.unshift(element);
};

unshiftMethod("mcdo");
unshiftMethod(1);
console.log(numbers);

// Arrangement of the elements
// ASCENDING ORDER

let numbs = [15, 27, 32, 12, 6, 8, 236];
console.log(numbs);

// sort method - arranges the elements in ascending or descending order. it has an anonymous function inside that has 2 parameters
	// anonymous function - unname function and can only be used once
		/*
			2 parameters inside the anonymous function represents: 
				first parameter - start / smallest value
				second parameter - end / biggest value
		*/

/*
	SYNTAX:
		arrayName.sort(
			function(a, b){
				statements
					[a - b] - ascending
					[b - a] - descending
			}
		)
*/
numbs.sort(
	function(a, b){
		return a - b;
	}
);
console.log(numbs);

// descending order
numbs.sort(
	function(a, b){
		return b - a
	}
);
console.log(numbs);

// reverse method - reverses the order of the elements in an array. it will depend on the last arrangement of the array, regardless if it is ascending, descending, or in random order.

numbs.reverse();
console.log(numbs);


// ================
// splice method - ctrl - x + ctrl - v
/*
	-directly manipulates the array
	-first parameter - the index of the element from which the omitting will begin.
	-second parameter - determines the number of elements to be omitted.
	-third parameter onwards - the replacements for the removed elements

		SYNTAX:
			let/const <newArray>=<originalArrayName>.splice(firstParameter, secondParameter, thirdParameter);
*/

// one parameter: (pure omission)
// let nums = numbs.splice(1);

// two parameters: (pure omission)
// let nums = numbs.splice(0, 2);

// three parameters: (replacements)
let nums = numbs.splice(4, 2, 31, 11, 111);
console.log(numbs);
console.log(nums);


// ================
// slice method - ctrl - c + ctrl + v
/*
	does not affect the original array


		SYNTAX:
			let/const <newArray>=<originalArrayName>.slice(firstParameter, secondParameter);
*/

// one parameter
// let slicedNums = numbs.slice(1);

// two parameters
let slicedNums = numbs.slice(4, 6);
console.log(numbs);
console.log(slicedNums);


// ================
// merging of array
// Concat
console.log(numbers);
console.log(numbs);
let animals = ["dog", "tiger", "kangaroo", "chicken"];
console.log(animals);

let newConcat = numbers.concat(numbs, animals);

console.log(newConcat);
console.log(numbers);
console.log(numbs);
console.log(animals);

// join method
let meal = ["rice", "steak", "juice"];

let newJoin = meal.join();
console.log(newJoin);

newJoin = meal.join("");
console.log(newJoin);

newJoin = meal.join(" ");
console.log(newJoin);

newJoin = meal.join("-");
console.log(newJoin);

// toString method - converts the element into string data type
console.log(nums);
// typeof determines the data type of the element after it
console.log(typeof nums);

let newString = numbs.toString();
console.log(typeof newString);

/*Accessors*/
let countries = ["US", "PH", "JP", "HK", "SG", "PH", "NZ"];
// indexOf - the first index it finds from the beginning of the array

// indexOf arrayName.indexOf()
let index = countries.indexOf("PH");
console.log(index);

// finding a non-existent element
index = countries.indexOf("AU");
console.log(index);

// lastIndexOf() - finds the index of the element starting from the end of the array;

/*
	SYNTAX
		arrayName.lastIndexOf()
*/

// returns -1 for non-existing elements
index = countries.lastIndexOf("PH");
console.log(index);


// using selection control structure

if (countries.indexOf("CAN") === -1) {
	console.log("Element not existing");
}else{
	console.log("Element exists in the array");
}

// Iterators

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
console.log(days)


// forEach - returns (performs the statement in) each element in the array
/*
	SYNTAX:
		array.forEach(
			function(element){
				statement/s
			}
		)

*/

days.forEach(
		function(element){
			console.log(element)
		}
	)

// map
/*
	SYNTAX:
		array.map(
			function(element){
				statement/s
			}
		)
*/

let mapDays = days.map(
		function(element){
			return `${element} is the day of the week.`
		}
	)

console.log(mapDays);
console.log(days);

// filter
console.log(nums);
let newFilter = numbs.filter(
		function(element){
			return element < 30
		}
	)

console.log(newFilter);
console.log(numbs);

// includes - return true (boolean) if the element/s are inside the array

let animalIncludes = animals.includes("dog");
console.log(animalIncludes);

// every - checks if all the elements pass the condition ( returns true if all of them does ).
console.log(nums);
let newEvery = nums.every(
	function(element){
		return (element > 10)
	}
)

console.log(newEvery);


// some - checks if at least 1 element passes the condition
let newSome = nums.some(
		function(element){
			return(element > 30)
		}
	)
console.log(newSome);

nums.push(50);
// reduce - performs the operation in all of the elements in the array
// first parameter - first element
// second parameter - last element
 let newReduce = nums.reduce(
 		function ( a, b ) {
 			return a + b
 		}
 	)
 console.log(newReduce);

 let average = newReduce/nums.length
 console.log(average);

// toFixed - sets the number of decimal places
console.log(average.toFixed(2));

/*
	parseInt - rounds the number to the nearest whole number
	parseFloat - rounds the number to the nearest target decimal places (through the use of .toFixed)
*/

console.log(parseInt(average.toFixed(2)));
console.log(parseFloat(average.toFixed(2)));